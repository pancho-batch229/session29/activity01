/*db.courses.insertMany([

    {
        name: "HTML Basics",
        price: 20000,
        isActive: true,
        instructor: "Sir Rome"
    },
    {   
        name: "CSS 101 + Flexbox",
        price: 21000,
        isActive: true,
        instructor: "Sir Rome"
    },
    {   
        name: "Javascript 101",
        price: 32000,
        isActive: true,
        instructor: "Ma'am Tine"
    },
    {   
        name: "Git 101, IDE and CLI",
        price: 19000,
        isActive: false,
        instructor: "Ma'am Tine"
    },
    {   
        name: "React.Js 101",
        price: 25000,
        isActive: true,
        instructor: "Ma'am Miah"
    }


])

db.courses.find({$and:[{instructor:"Sir Rome"},{price:{$gte:20000}}]})

db.courses.find({instructor:"Ma'am Tine"},{name:1,_id:0})

db.courses.find({instructor:"Ma'am Miah"},{_id:0,instructor:1,price:1})

db.courses.find({$and:[{instructor:{$regex:'r',$options:'$i'}},{price:{$gte:20000}}]})

db.courses.find({$and:[{isActive:true},{price:{$lte:25000}}]})

*/